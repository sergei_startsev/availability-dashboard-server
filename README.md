# OVERVIEW
Tool for tracking Cobalt infrastructure availability.

# HOW TO

## PRECONDITION

* Clone this repo.
* Be sure that you've installed [Node.js](https://docs.npmjs.com/getting-started/installing-node) and [tsc](https://www.typescriptlang.org/).
* Restore packages from `package.json` using following command:

    `npm install`

from inside your app directory (i.e. where package.json is located). After you run that command `node_modules` directory should be added to your app directory.

## BUILD
To build the source code run the following command:

`tsc src/index.ts`

Then you could find the compiled typescript code in `src` dir (*.js files).

## START SERVER

To start server run the following command:

`node src/index.js`

It starts the server and opens socket connection on `*:3000`. 