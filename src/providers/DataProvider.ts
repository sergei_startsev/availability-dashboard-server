import {IDataProvider} from "./iDataProvider";
import {ProductBriefStatistics} from "../models/productBriefStatistics";
import {BriefStatistics} from "../models/briefStatistics";

export class DataProvider implements IDataProvider {
    private static productIDs: number[] = [1];
    private static productNames: string[] = ["WLN"];
    private static envs: string[] = ["CI", "DEMO", "QED", "HOTPROD", "PROD"];
    private static modulesCountMin: number = 1;
    private static modulesCountMax: number = 30;

    public getBriefStatistics(): Array<ProductBriefStatistics> {
        var statistics: Array<ProductBriefStatistics> = new Array<ProductBriefStatistics>();
        for (var i: number = 0; i < DataProvider.productIDs.length; i++) {
            DataProvider.envs.forEach(env => {
                statistics.push(new ProductBriefStatistics(
                    DataProvider.productIDs[i],
                    DataProvider.productNames[i],
                    env,
                    new BriefStatistics(
                        this.getRandom(DataProvider.modulesCountMin, DataProvider.modulesCountMax),
                        this.getRandom(DataProvider.modulesCountMin, DataProvider.modulesCountMax),
                        this.getRandom(DataProvider.modulesCountMin, DataProvider.modulesCountMax),
                        this.generate24hData(DataProvider.modulesCountMin, DataProvider.modulesCountMax)
                        )
                    ));
            });
        }

        return statistics;
    }

    private getRandom(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1) + min);;
    }

    private generate24hData(min: number, max: number): number[] {
        var data = [];
        for (var i = 0; i < 24; i++) {
            data.push(this.getRandom(min, max))
        }
        return data;
    }
}