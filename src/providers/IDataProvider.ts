import {ProductBriefStatistics} from "../models/productBriefStatistics";

export interface IDataProvider{
    getBriefStatistics(): any;
} 