import {IDataProvider} from "./iDataProvider";
import {ProductBriefStatistics} from "../models/productBriefStatistics";
import {BriefStatistics} from "../models/briefStatistics";

export class SmartDataProvider implements IDataProvider {
    private static productIDs: number[] = [1];
    private static productNames: string[] = ["WLN"];
    private static envs: string[] = ["CI", "DEMO", "QED", "HOTPROD", "PROD"];
    private static modulesCountMin: number = 1;
    private static modulesCountMax: number = 30;

    private previousState: Array<ProductBriefStatistics>;

    public getBriefStatistics(): Array<ProductBriefStatistics> {
        if (this.previousState == null) {
            this.previousState = this.getInitVersion();
        } else {
            this.previousState.forEach(productBriefStatistis => {
                productBriefStatistis.statistics.availableModulesCount = this.getRandomSmart(productBriefStatistis.statistics.availableModulesCount)
                productBriefStatistis.statistics.brokenModulesCount = this.getRandomSmart(productBriefStatistis.statistics.brokenModulesCount)
                productBriefStatistis.statistics.undefinedModulesCount = this.getRandomSmart(productBriefStatistis.statistics.undefinedModulesCount)
                productBriefStatistis.statistics.availability24h = this.generate24hSmartData(productBriefStatistis.statistics.availability24h)
            });
        }
        return this.previousState;
    }

    private getRandom(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1) + min);;
    }

    private generate24hData(min: number, max: number): number[] {
        var data = [];
        var previous = 0;
        for (var i = 0; i < 24; i++) {
            if(previous!==0){
                if (Math.random() > 0.5 && previous < 100) {
                    previous += this.getRandom(1, 2);
                } else {
                    if (previous > 0) {
                        previous -= this.getRandom(1, 2)
                    }
                }
            }else{
                previous = this.getRandom(min, max);
            }
            data.push(previous)
        }
        return data;
    }

    private generate24hSmartData(currentValues: number[]): number[] {
        if (Math.random() < 0.3) {
            currentValues.shift();
            currentValues.push(this.getRandom(80,100));
            return currentValues;
        } else {
            return currentValues
        }
    }

    private getRandomSmart(currentValue: number): number {
        if (Math.random() > 0.5 && currentValue < 50) {
            return (currentValue + this.getRandom(1, 2));
        } else {
            if (currentValue > 0) {
                var newValue = currentValue - this.getRandom(1, 2)
                if (newValue > 0) {
                    return newValue;
                } else {
                    return currentValue;
                }
            } else {
                return currentValue;
            }
        }
    }

    private getInitVersion(): Array<ProductBriefStatistics> {
        var statistics: Array<ProductBriefStatistics> = new Array<ProductBriefStatistics>();
        for (var i: number = 0; i < SmartDataProvider.productIDs.length; i++) {
            SmartDataProvider.envs.forEach(env => {
                statistics.push(new ProductBriefStatistics(
                    SmartDataProvider.productIDs[i],
                    SmartDataProvider.productNames[i],
                    env,
                    new BriefStatistics(
                        this.getRandom(SmartDataProvider.modulesCountMax/2, SmartDataProvider.modulesCountMax),
                        this.getRandom(SmartDataProvider.modulesCountMin, SmartDataProvider.modulesCountMax/5),
                        this.getRandom(SmartDataProvider.modulesCountMin, SmartDataProvider.modulesCountMax/5),
                        this.generate24hData(80, 100)
                        )
                    ));
            });
        }

        return statistics;
    }
}