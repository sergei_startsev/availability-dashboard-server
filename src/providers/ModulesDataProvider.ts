import { IDataProvider } from './iDataProvider';
import { ProductBriefStatistics } from "../models/productBriefStatistics";
import { Module } from '../models/module';
import { Environment } from '../models/environment';
import { ServerAvailability } from '../models/serverAvailability';
import { ServerAvailabilityData } from '../models/serverAvailabilityData';
import { Server } from '../models/server';
/**
 * ModulesDataProvider
 */
export class ModulesDataProvider implements IDataProvider {
    private static modulesNames: string[] = ["Alerts"];
    private static moduleTypes: string[] = ["tomcat"];
    private static productFamiliesNames: string[] = ["WLN"];
    private static envs: string[] = ["CI", "DEMO", "QED", "HOTPROD", "PROD"];
    private initAvailability = null;

    public getBriefStatistics(): Array<ProductBriefStatistics> {
        let data = [];
        for (let i = 0; i < ModulesDataProvider.modulesNames.length; i++) {
            let envs = [];
            for (let j = 0; j < ModulesDataProvider.envs.length; j++) {
                if (this.initAvailability == null) {
                    this.initAvailability = this.generateServerAvailability();
                }
                envs.push(new Environment(
                    ModulesDataProvider.envs[j],
                    "29.2.2",
                    (new Date).getTime(),
                    this.initAvailability
                ));
            }
            data.push(new Module(
                ModulesDataProvider.modulesNames[i],
                ModulesDataProvider.productFamiliesNames[0],
                ModulesDataProvider.moduleTypes[0],
                envs
            ));
        }
        return data;
    }
    
    private generateServerAvailability(){
        let availability = [];
        let data = this.generateFakeData();
        
        availability.push(new ServerAvailability(
            "general",
            null,
            new ServerAvailabilityData(data.data.labels,data.data.series)
        ));
        availability.push(new ServerAvailability(
            "cb0001-03",
            new Server("cb0001-03", 1),
            new ServerAvailabilityData(data.data2.labels,data.data2.series)
        ));
        availability.push(new ServerAvailability(
            "cb0001-04",
            new Server("cb0001-04", 1),
            new ServerAvailabilityData(data.data3.labels,data.data3.series)
        ));
        
        return availability;
    }
    
    private getRandom(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1) + min);;
    }
    
    private generateFakeData() {
        let data = { labels: [], series: [] }
        let dataSerie = [];
        for (let i = 1; i <= 24; i++) {
            let hours;
            if (i <= 12) {
                hours = i + "am";
            } else {
                hours = i - 12 + "pm";
            }
            data.labels.push(hours);
            if (this.getRandom(0, 10) < 8) {
                dataSerie.push(100);
            } else {
                dataSerie.push(this.getRandom(0, 100));
            }
        }

        let dataSerie2 = [], dataSerie3 = [];
        for (let i = 0; i < 24; i++) {
            let diff = 100 - dataSerie[i];
            if (diff > 0) {
                if (this.getRandom(0, 10) < 8) {
                    dataSerie2.push(diff);
                    dataSerie3.push(0);
                } else {
                    dataSerie2.push(0);
                    dataSerie3.push(diff);
                }
            } else {
                dataSerie2.push(0);
                dataSerie3.push(0);
            }
        }
        data.series.push(dataSerie);
        data.series.push(dataSerie2);
        data.series.push(dataSerie3);


        let data2 = { labels: data.labels, series: [] }
        let data3 = { labels: data.labels, series: [] }
        dataSerie = [], dataSerie2 = [], dataSerie3 = [];
        let dataSerie4 = [], dataSerie5 = [], dataSerie6 = [];
        for (let i = 0; i < 24; i++) {
            let success = data.series[0][i];
            let fail = data.series[1][i];
            let unknown = data.series[2][i];
            if (success != 100) {
                if (success <= 50) {
                    dataSerie[i] = success * 2;
                    dataSerie4[i] = 0;
                } else {
                    dataSerie[i] = 100;
                    dataSerie4[i] = success * 2 - 100;
                }
                if (fail * 2 + dataSerie[i] <= 100) {
                    dataSerie2[i] = fail * 2;
                    dataSerie5[i] = 0;
                } else {
                    dataSerie2[i] = 100 - dataSerie[i];
                    dataSerie5[i] = fail * 2 - (100 - dataSerie[i]);
                }

                if (unknown * 2 + dataSerie[i] <= 100) {
                    dataSerie3[i] = unknown * 2;
                    dataSerie6[i] = 0;
                } else {
                    dataSerie3[i] = 100 - dataSerie[i];
                    dataSerie6[i] = unknown * 2 - (100 - dataSerie[i]);
                }
            } else {
                dataSerie[i] = 100;
                dataSerie2[i] = 0;
                dataSerie3[i] = 0;
                dataSerie4[i] = 100;
                dataSerie5[i] = 0;
                dataSerie6[i] = 0;
            }
        }

        data2.series.push(dataSerie);
        data2.series.push(dataSerie2);
        data2.series.push(dataSerie3);

        data3.series.push(dataSerie4);
        data3.series.push(dataSerie5);
        data3.series.push(dataSerie6);

        return { data: data, data2: data2, data3: data3 };
    }

}