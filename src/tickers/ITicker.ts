export interface Subscriber {
    (data: any);
}

export interface ITicker{
    
    subscribe(subscriber: Subscriber): boolean;
    unsubscribe(subscriber: Subscriber): boolean;
    notify(): void;
    
}