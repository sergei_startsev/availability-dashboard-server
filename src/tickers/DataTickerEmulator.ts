import {ITicker, Subscriber} from "./iTicker";
import {IDataProvider} from "../providers/iDataProvider";
import {DataProvider} from "../providers/dataProvider";
import {SmartDataProvider} from "../providers/smartDataProvider";

export class DataTickerEmulator implements ITicker {
    private dataProvider: IDataProvider;
    private subscribers: Array<Subscriber> = new Array<Subscriber>();
    public static get FREQUENCY_UPDATE(): number { return 500; }

    constructor(dataProvider?: IDataProvider) {
        this.dataProvider = dataProvider;
        setInterval(() => { this.notify() }, DataTickerEmulator.FREQUENCY_UPDATE);
    }

    private static instance: ITicker;

    public static getInstance() {
        if (DataTickerEmulator.instance == null) {
            DataTickerEmulator.instance = new DataTickerEmulator(new SmartDataProvider());
        }
        return DataTickerEmulator.instance;
    }

    public subscribe(subscriber: Subscriber) {
        this.subscribers.push(subscriber);
        return true;
    }

    public unsubscribe(subscriber: Subscriber) {
        if (this.subscribers.splice(this.subscribers.indexOf(subscriber), 1)
            .length > 0) {
            return true;
        } else {
            return false;
        }

    }

    public notify() {
        console.log("Subscribers: " + this.subscribers.length);
        var data = this.dataProvider.getBriefStatistics();
        this.subscribers.forEach(subscriber => {
            subscriber(data);
        });
    }
}