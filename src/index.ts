import * as App from "express";
App = App();

import * as Http from "http";
var httpServer = Http.Server(App);

import * as Socket from "socket.io";
var io = Socket(httpServer);

import {DataTickerEmulator} from "./tickers/dataTickerEmulator";
import {SmartDataProvider} from "./providers/smartDataProvider";
import {ModulesDataProvider} from "./providers/modulesDataProvider";

var dataTicker4Products = new DataTickerEmulator(new SmartDataProvider()),
    dataTicker4Modules = new DataTickerEmulator(new ModulesDataProvider());

App.get("/", function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
    console.log('a user connected');

    var productsSubscriber = function(data) {
        socket.emit('briefStatistics', data);
    };
    
    dataTicker4Products.subscribe(productsSubscriber);
    
    var modulesSubscriber = function(data) {
        socket.emit('modulesStatistics', data);
    };
    
    dataTicker4Modules.subscribe(modulesSubscriber);
    
    socket.on('disconnect', function() {
        console.log('user disconnected');
        dataTicker4Products.unsubscribe(productsSubscriber);
        dataTicker4Modules.unsubscribe(modulesSubscriber);
    });
});

httpServer.listen(3000, function() {
    console.log("listening on *:3000");
});