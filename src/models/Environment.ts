import { ServerAvailability } from './serverAvailability';
/**
 * Environment
 */
export class Environment {
    
    public envName: string;
    public version: string;
    public date: number;
    public availability: Array<ServerAvailability>;
    
    constructor(
        envName: string,
        version: string,
        date: number,
        availability: Array<ServerAvailability>
        ) {
        this.envName = envName;
        this.version = version;
        this.date = date;
        this.availability = availability;
    }
}