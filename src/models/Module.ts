import { Environment } from './environment';

/**
 * Module
 */
export class Module {

    public moduleName: string;
    public productFamily: string;
    public type: string;
    public environments: Array<Environment>

    constructor(
        moduleName: string,
        productFamily: string,
        type: string,
        environments: Array<Environment>
        ) {
        this.moduleName = moduleName;
        this.productFamily = productFamily;
        this.type = type;
        this.environments = environments;
    }
}