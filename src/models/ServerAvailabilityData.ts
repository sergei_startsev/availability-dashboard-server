/**
 * ServerAvailabilityData
 */
export class ServerAvailabilityData {

    public labels: Array<string>;
    public series: Array<Array<number>>;

    constructor(labels: Array<string>, series: Array<Array<number>>){
        this.labels = labels;
        this.series = series;
    }
}