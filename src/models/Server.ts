/**
 * Server
 */
export class Server {
    
    public name: string;
    public status: number;
    
    constructor(name: string, status: number){
        this.name = name;
        this.status = status;
    }
}