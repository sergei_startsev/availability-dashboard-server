import { Server } from './server';
import { ServerAvailabilityData } from './serverAvailabilityData';

/**
 * ServerAvailability
 */
export class ServerAvailability {
    
    public name: string;
    public server: Server;
    public data: ServerAvailabilityData;

    constructor(
        name: string,
        server: Server,
        data: ServerAvailabilityData
        ) {
        this.name = name;
        this.server = server;
        this.data = data;
    }
}