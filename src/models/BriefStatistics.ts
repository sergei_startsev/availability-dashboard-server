export class BriefStatistics{
    
    public availableModulesCount: number;
    public brokenModulesCount: number;
    public undefinedModulesCount: number
    public availability24h: Array<number>;
    
    constructor(availableModulesCount?:number,
                brokenModulesCount?: number,
                undefinedModulesCount?: number,
                availability24h?: Array<number>){
                    
        this.availableModulesCount = typeof availableModulesCount !== 'undefined' ? availableModulesCount : 0;
        this.brokenModulesCount = typeof brokenModulesCount !== 'undefined' ? brokenModulesCount : 0;
        this.undefinedModulesCount = typeof undefinedModulesCount !== 'undefined' ? undefinedModulesCount : 0;
        this.availability24h = typeof availability24h !== 'undefined' ? availability24h : [];
    }
    
}